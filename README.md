# Instructions for running the Bookmarklet

1. Code should be written in a function that calls itself. This makes sure the variables go out of scope at the end.

```js
(function() { code; })()
```

2. Open the browser console (`Ctrl+Shift+I -> Console`) and run the `encodeURIComponent`. This allows us to use whitespace and other special characters not allowed in the URL bar.

```js
encodeURIComponent(`YOUR CODE HERE`)
```

3. Copy the result and create a new bookmark..

4. Replace the bookmark URL with `javascript:` followed by your encoded JS code.

5. Click the bookmark to run it!