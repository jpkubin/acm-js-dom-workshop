(function () {
    "use strict"; /*The strict mode directive tells the browser to raise errors that would have been 'silent'.*/

    /* These are some CSS selectors on eBay's price listings that the JavaScript can access */
    const PRICE = ".s-item__price";
    const SHIPPING = ".s-item__shipping";
    const ITEM = ".s-item";
    const PRICE_REGEX = /[0-9]+.[0-9]+/g;

    /* Creates a "list" of all elements selected by '.s-item' */
    let items = document.querySelectorAll(ITEM);

    /* We can use the window object to set global variables for the entire page */
    /* In this context we want to know whether the script has already run */
    if (window.fixedPrices) {
        console.log("Prices have already been fixed!");
        return;
    }
    
    /* for-of loops are used to iterate through an array */
    for (let listing of items) {
        /* we can also use querySelector on elements themselves */
        let shipping = listing.querySelector(SHIPPING);
        let shippingcost = shipping?.innerHTML?.match(PRICE_REGEX)?.[0];
        if (shippingcost) {
            /* cast to float */
            shippingcost = parseFloat(shippingcost);

            /* sometimes the listing has multiple prices, so we need to select all of them */
            let prices = listing.querySelectorAll(PRICE);
            
            for (let price of prices) {
                let originalPrice = price.textContent;

                /* replace each of the prices in the text node with the prices added together*/
                price.innerHTML = price.innerHTML.replace(PRICE_REGEX, match => {
                    let itemcost = parseFloat(match);
                    return (itemcost + shippingcost).toFixed(2);
                });

                price.onclick = () => alert("Listed at "+originalPrice);

                price.style.color = 'purple';
            }
            shipping.style.textDecoration = 'line-through';
        }
    }
    window.fixedPrices = true;
})()