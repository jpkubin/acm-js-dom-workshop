(function () {
    "use strict"; /*The strict mode directive tells the browser to raise errors that would have been 'silent'.*/

    /* These are some CSS selectors on eBay's price listings that the JavaScript can access */
    const PRICE = ".s-item__price";
    const SHIPPING = ".s-item__shipping";
    const ITEM = ".s-item";

    const PRICE_REGEX = /[0-9]+.[0-9]+/g;

    /* 
    
    TASK ONE:
    Create a list of *all* elements selected by '.s-item' 
    
    */
    let items = /* your code here */;

    /* 

    FINAL TASK (come back to me):

    We can use the `window` object to set global variables for the entire page
    In this context we want to know whether the script has already run
    */
    if (/* your code */) {
        /*
        Send an alert or a log that the program has already run
        */
        
        return;
    }
    
    /* 
    
    TASK TWO:
    Loop through the list of items we just made.
    
    */
    for (let listing/* your code */) {


        /* 

        TASK THREE:
        Select the shipping information WITHIN the current listing.
        (hint 1, use the constant at the top with the CSS selector)
        (hint 2, there should only be 1 shipping element per listing)
        
        */
        let shipping = /* your code */;

        /*
        Using regex to see if there is a substring that looks like a price (0.00, etc.) 
        */
        let shippingcost = shipping?.innerHTML?.match(PRICE_REGEX)?.[0];
        if (shippingcost) { /*Run only if the shipping cost is not undefined*/
            /* cast to float */
            shippingcost = parseFloat(shippingcost);

            /* 

            TASK FOUR: 
            Select ALL elements matching the selector in PRICE within the current listing.
            
            */
            let prices = /* your code */
            
            for (let price of prices) {
                /*
                
                TASK FIVE:
                Select get the text contained by the 'price' element

                */
                let originalPrice = /* your code */

                /* replace each of the prices in the text node with the prices added together*/
                price.innerHTML = price.innerHTML.replace(PRICE_REGEX, match => {
                    let itemcost = parseFloat(match);
                    return (itemcost + shippingcost).toFixed(2);
                });

                /*

                TASK SIX:
                Now that we've corrected the price, set the price 
                element's CSS color to your favorite happy color
                
                */
                price./*your code*/

                /*

                TASK 
                
                */
                price.onclick = /* your code*/
            }
            shipping.style.textDecoration = 'line-through';
        }
    }
    /*

    TASK EIGHT:
    Set the pricesFixed property to true for the window. Now do the final task!

    */
    window.
})()